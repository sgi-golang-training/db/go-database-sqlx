Step 1
```
go get github.com/jmoiron/sqlx
```

Step 2
Set credential
```
db, err := sqlx.Connect("mysql", "godev:godev@tcp(127.0.0.1:3306)/dbgo")
```

Step 3
Run
```
go run main.go
```